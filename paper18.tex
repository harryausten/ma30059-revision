\documentclass{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage[shortlabels]{enumitem}
\usepackage{mathtools}
\usepackage{cancel}

\renewcommand  {\a}{{\bm a}}
\newcommand  {\C}{\mathcal{C}}
\renewcommand{\d}{{\ \mathrm{d}}}
\newcommand  {\e}{\epsilon}
\newcommand  {\D}{\Delta}
\newcommand  {\F}{{\bm F}}
\newcommand  {\n}{{\bm n}}
\renewcommand{\o}{\omega}
\renewcommand{\O}{\Omega}
\newcommand  {\Ob}{{\bar{\Omega}}}
\newcommand  {\p}{\partial}
\newcommand  {\pO}{{\partial\Omega}}
\renewcommand{\r}{{\bm r}}
\newcommand  {\R}{\mathbb{R}}
\newcommand  {\x}{{\bm x}}
\newcommand  {\y}{{\bm y}}
\newcommand  {\z}{{\bm z}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{MA30059 Mathematical Methods II}}

    {\large \textbf{2018 Paper}}
\end{center}

\begin{enumerate}

    % Question 1
    \item Let $ \O \subset \R^m $ be a bounded open set with sufficiently smooth boundary $\pO$ for application of the divergence theorem and let $ u \in C^2(\O) $
        \begin{enumerate}
            % Part A
            \item
                \begin{enumerate}[(i)]
                    \item State what it means for $u$ to be a harmonic function in $\O$, i.e. state the equation $u$ satisfies
                \end{enumerate}
                \begin{itemize}
                    \item $ \D u = 0 \quad \text{in } \O $
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item State (without proof) the Mean Value Property (MVP) satisfied by a harmonic function $u$
                \end{enumerate}
                \begin{itemize}
                    \item Let $ u \in C^2(\O) $ be harmonic and consider $ \overline{B(\x,\rho)} \subset \O $ for $ \x \in \O $ and $ \rho > 0 $. Then
                        $$ u(\x) = \frac{1}{\o_m\rho^{m-1}} \int_{\lvert \y-\x \rvert = \rho} u(\y) \d S(\y) $$
                        where $\o_m$ is the area of the \textbf{unit sphere} in $m$ dimensions
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item State and prove the strong maximum principle for harmonic functions in the bounded domain $\O$
                \end{enumerate}
                \begin{itemize}
                    \item Let $ \O \subset \R^m $ be open, bounded and connected and $ u \in C(\Ob) \cap C^2(\O) $ be harmonic. Then
                        \begin{equation*}
                            \text{either}
                            \begin{cases}
                                u \text{ is constant} \\
                                u(\x) < \max_{y \in \Ob} u(\y), \quad \forall \x \in \O
                            \end{cases}
                        \end{equation*}

                        \underline{\textbf{Proof}}

                        Let
                        \begin{align*}
                            M &= \max_{\y \in \Ob} u(\y), \\
                            \O_0 &= \{ \y \in \O : u(\y) = M \} \\
                            \text{and} \quad \O_1 &= \{ \y \in \O : u(\y) < M \}
                        \end{align*}
                        Then $ \O = \O_0 \cup \O_1 $ and $ \O_0 \cap \O_1 = \emptyset $. $\O_1$ is open by continuity of $u$ (Inertia principle for $M-u$)

                        For arbitrary $ \x \in \O_0 $,
                        $$ \exists \rho_0 > 0 \text{ such that } B(\x,\rho_0) \subset \O $$
                        since $\O$ is open. For $ 0 < \rho < \rho_0 $
                        \begin{align*}
                            \text{MVP} &\implies u(\x) - \frac{1}{\o_m\rho^{m-1}} \int_{\lvert \y - \x \rvert = \rho} u(\y) \d S(\y) = 0 \\
                            &\implies \frac{1}{\o_m\rho_{m-1}} \int_{\lvert \y - \x \rvert = \rho} M - u(\y) \d S(\y) = 0 \\
                            &\implies M - u(\y) = 0 \quad \text{on } \lvert \y - \x \rvert = \rho
                        \end{align*}
                        since $ M - u(\y) $ is continuous and nonnegative
                        \begin{align*}
                            &\implies M - u(\y) = 0 \quad \forall \y \in B(\x,\rho_0) \\
                            &\implies \O_0 \text{ is open}
                        \end{align*}
                        But $\O$ cannot be represented as a disjoint union of two non-empty sets $\O_0$ and $\O_1$. Hence
                        \begin{equation*}
                            \text{either}
                            \begin{cases}
                                \O_1 = \emptyset \implies \text{(i) holds} \\
                                \O_0 = \emptyset \implies \text{(ii) holds}
                            \end{cases}
                        \end{equation*}
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Let $ \O = \{ \x = (x_1,x_2) \in \R^2 : \lvert \x \rvert < 1 \} $ and $ u \in C(\Ob) \cap C^2(\O) $ be a harmonic function with boundary condition
                        $$ u(\x) = x_1^2 - x_2^2, \quad \x \in \pO $$
                        Prove that $ -1 < u(\x) < 1, \ \forall x \in \O $ and find the values of $ u(\bm{0}) $
                \end{enumerate}
                \begin{itemize}
                    \item Parameterise $\pO$ as
                        $$ x_1 = \cos\theta, \quad x_2 = \sin\theta $$
                        Hence, on $\pO$
                        \begin{align*}
                            u &= \cos^2\theta - \sin^2\theta \\
                            &= \cos 2\theta \\
                            \theta \in [0,2\pi] &\implies -1 \le u \le 1
                        \end{align*}
                        Hence $u$ is not constant, therefore by the Strong Maximum Principle,
                        $$ u(\x) < \max_\pO u = 1, \quad \forall \x \in \O $$
                        Similarly
                        $$ -u(\x) < \max_\pO -u = 1 \implies u(\x) > -1, \quad \forall \x \in \O $$
                        Thus $ -1 < u(\x) < 1 $, $ \forall \x \in \O $ and
                        $$ u(\bm{0}) = \frac{1}{2\pi} \int_0^{2\pi} \cos 2\theta \d\theta = 0 $$
                        using the MVP
                \end{itemize}
            % Part B
            \item This part of the question should be done from first principles, without using the strong or weak maximum principles
                \begin{enumerate}[(i)]
                    \item Let $ u \in C(\Ob) \cap C^2(\O) $ be such that $ \Delta u < 0 $ in $\O$. Show that
                        \begin{equation} \label{minmax}
                            \min_{\x \in \Ob} u(\x) = \min_{\x \in \pO} u(\x)
                        \end{equation}
                        [Hint: Argue by contradiction]
                \end{enumerate}
                \begin{itemize}
                    \item $$ \pO \subset \Ob \implies \min_{\Ob} u \le \min_{\pO} u $$
                        Suppose $ \min_{\Ob} u < \min_{\pO} u $, i.e. $ \exists \x_0 \in \O $ such that
                        $$ u(\x_0) \le u(\x), \quad \forall \x \in \Ob $$
                        Since $\x_0$ is a global minimum (interior), it is also a local minimum, necessary conditions for which are
                        \begin{align*}
                            \frac{\p^2 u}{\p x_i^2}(\x_0) &\ge 0, \quad i = 1, \hdots, m \\
                            \implies \D u(\x_0) &= \sum_{i=1}^m \frac{\p^2 u}{\p x_i^2}(\x_0) \ge 0
                        \end{align*}
                        which is a contradiction, since
                        $$ \D u < 0, \quad \forall \x \in \O $$
                        Thus $u$ cannot have a minimum at any interior point and
                        $$ \min_{\Ob} u = \min_{\pO} u $$
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Now let $ u \in C(\Ob) \cap C^2(\O) $ be such that $ \Delta u \le 0 $ in $\O$. By considering a function of the form $ u(\x) - \epsilon \lvert \x \rvert^2 $ for $ \epsilon > 0 $, or otherwise, show that \eqref{minmax} still holds
                \end{enumerate}
                \begin{itemize}
                    \item Let $ u_\epsilon(\x) = u(\x) - \epsilon\lvert\x\rvert^2 $ for some $ \epsilon > 0 $. Then
                        $$ \D u_\epsilon = \D u - 2m\epsilon < 0 $$
                        since $ \D u \le 0 $ and $ \epsilon > 0 $. Then by part (i),
                        $$ \min_\Ob u_\epsilon = \min_\pO u_\epsilon $$
                        Now, for any $ \x \in \O $
                        \begin{align*}
                            u(\x) &\ge u_\e(\x) \\
                            &\ge \min_\pO u_\e \\
                            &\ge \min_\pO u - \e R^2 \\
                            \text{where} \quad R &= \max_{\y \in \pO} \lvert \y \rvert \\
                            \implies \min_\pO u &\le \min_\Ob u \\
                            \text{but} \quad \pO \subset \Ob &\implies \min_\pO u \ge \min_\Ob u \\
                            &\implies \min_\Ob u = \min_\pO u
                        \end{align*}
                \end{itemize}
        \end{enumerate}

    % Question 2
    \item Let $ \O \subset \R^m $ be a bounded, open and connected set, with $\pO$ smooth enough for the divergence theorem to be applied. Consider the following problem for $ u \in C^2(\Ob) $
        \begin{align}
            \label{p1} \Delta u(\x) &= f(\x) \quad \forall \x \in \O \\
            \label{p2} \frac{\p u}{\p \n}(\x) + \alpha u (\x) &= g(\x) \quad \forall \x \in \pO
        \end{align}
        with given $ f \in C(\Ob), g \in C(\pO) $ and constant $ \alpha \ge 0 $. Here, $\displaystyle\frac{\p u}{\p \n}$ denotes the usual outward normal derivative

        \begin{enumerate}
            % Part A
            \item State the divergence theorem and use it to derive Green's first identity for $ u, v \in C^2(\Ob) $
                \begin{itemize}
                    \item \textbf{Divergence Theorem}: Let $ \F : \O \rightarrow \R^m $ be $ C'(\Ob) $, then
                        $$ \int_\O \nabla \cdot \F \d\x = \int_\pO \F \cdot \n \d S $$
                        Let $ \F = v \nabla u $, then
                        $$ \nabla \cdot \F = v \D u + \nabla v \cdot \nabla u $$
                        $ u,v \in C^2(\Ob) \implies \F \in C'(\Ob) $, giving \textbf{Green's first identity}
                        $$ \int_\O v \D u + \nabla v \cdot \nabla u \d\x = \int_\pO v \frac{\p u}{\p\n} \d S $$
                        where $ \displaystyle\frac{\p u}{\p\n} = \n \cdot \nabla u $
                \end{itemize}
            % Part B
            \item Prove that the solution to \eqref{p1}$-$\eqref{p2} is unique for $ \alpha > 0 $
                \begin{itemize}
                    \item Let $ u_1,u_2 $ be solutions and set $ v = u_1 - u_2 $, then
                        \begin{align}
                            \label{v} \tag{*} \D v &= 0 \quad \text{in } \O \\
                            \notag \text{and} \quad \frac{\p v}{\p\n} + \alpha v &= 0 \quad \text{on } \pO
                        \end{align}
                        Since $ u_1, u_2 \in C^2(\Ob) $, then $ v \in C^2(\Ob) $ and Green's first identity gives
                        $$ \int_\O v \D v + \lvert \nabla v \rvert^2 \d\x = \int_\pO v \frac{\p v}{\p\n} \d S $$
                        Using \eqref{v}, we have
                        $$ \int_\O \lvert \nabla v \rvert^2 \d\x = -\alpha\int_\pO v^2 \d S $$
                        Since $ \alpha > 0 $,
                        \begin{align*}
                            \int_\O \lvert \nabla v \rvert^2 \d\x &= 0 \\
                            \text{and} \quad \int_\pO v^2 \d S &= 0
                        \end{align*}
                        Both integrals areb continuous over the closure of their domains, so the \textbf{Vanishing Theorem} gives
                        \begin{align*}
                            \lvert \nabla v \rvert^2 &= 0 \quad \text{in } \Ob \\
                            \text{and} \quad v^2 &= 0 \quad \text{on } \pO
                        \end{align*}
                        Thus $v=0$ in $\Ob$, since $\O$ is connected. Hence $u_1=u_2$ in $\Ob$, which gives uniqueness
                        \begin{flushright}
                            $\Box$
                        \end{flushright}
                \end{itemize}
            % Part C
            \item Now consider the case $ \alpha = 0 $ in the rest of this question
                \begin{enumerate}[(i)]
                    \item Explain brifely if the solution to \eqref{p1}$-$\eqref{p2} is unique
                \end{enumerate}
                \begin{itemize}
                    \item The solution is not unique, since $u$ plus a constant is a solution for any constant
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Derive a necessary condition involving $f$ and $g$ only, for a solution to \eqref{p1}$-$\eqref{p2} to exist
                \end{enumerate}
                \begin{itemize}
                    \item
                        \begin{align*}
                            \int_\O f \d\x &= \int_\O \nabla \cdot (\nabla u) \d\x \\
                            &= \int_\pO \frac{\p u}{\p\n}\d S \\
                            &= \int_\pO g \d S
                        \end{align*}
                        using the divergence theorem
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Show also that the solution $u$ to \eqref{p1}$-$\eqref{p2} satisfies
                        $$ \int_\pO u(\x)g(\x) \d S(\x) = \int_\O \lvert \nabla u(\x) \rvert^2 \d \x + \int_\O u(\x)f(\x) \d \x $$
                \end{enumerate}
                \begin{itemize}
                    \item
                        \begin{align*}
                            \int_\pO ug \d S &= \int_\pO u \frac{\p u}{\p\n} \d S \\
                            &= \int_\O \nabla \cdot (u\nabla u) \d\x \\
                            &= \int_\O u \D u + \lvert \nabla u \rvert^2 \d\x \\
                            &= \int_\O \lvert \nabla u \rvert^2 \d\x + \int_\O uf \d\x
                        \end{align*}
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Now consider the unit disc $ \O = \{ \x \in \R^2 : \lvert \x \rvert < 1 \} $, with
                        $$ f(\x) = \delta(\x - \y), \quad g(\x) = 1 $$
                        for fixed $ \y \in \O $ and $\delta$ being the (multivariate) Dirac-delta function. Show that \eqref{p1}$-$\eqref{p2} has a solution in the form
                        $$ u(\x) = N(\x-\y) + CN(\x-\r(\y)) $$
                        where $N$ is the Newtonian potential, $ \r(\y) = \y / \lvert \y \rvert^2 $ and $C$ is a constant to be determined [Properties of the Newtonian potenial and inverse points for circles may be used if clearly stated]
                \end{enumerate}
                \begin{itemize}
                    \item We have
                        \begin{align*}
                            \D u &= \delta(\x-\y) \quad \text{in } \O \\
                            \text{and} \quad \frac{\p u}{\p\n} &= 1 \quad \text{on } \pO
                        \end{align*}
                        Consider
                        $$ u = N(\x-\y) + CN(\x-\r(\y)) $$
                        where
                        $$ N(\x-\a) = \frac{1}{2\pi} \ln{\lvert \x - \a \rvert} $$
                        for $ \a = \y $ or $\r(\y)$. For $ \y \in \O $
                        \begin{align*}
                            \D_\x N(\x-\y) &= \delta(\x-\y) \\
                            \text{and} \quad \D_\x N(\x-\r(\y)) &= 0
                        \end{align*}
                        since $ \r(\y) \not\in \O $. Therefore
                        \begin{align*}
                            \D_\x u &= \D_\x N(\x-\y) + C\D_\x N(\x-\r(\y)) \\
                            &= \delta(\x-\y) \\
                            &\Rightarrow u \text{ satisfies \eqref{p1}}
                        \end{align*}
                        On the boundary
                        \begin{align*}
                            \frac{\p u}{\p\n} &= \n \cdot \nabla u \\
                            \text{and} \quad \n &= \frac{\x}{\lvert\x\rvert} \\
                            &= \x
                        \end{align*}
                        for $ \x \in \pO $ where $ \lvert\x\rvert = 1 $. Also
                        \begin{align*}
                            \nabla_\x N(\x-\a) &= \frac{1}{2\pi}\frac{\x-\a}{\lvert\x-\a\rvert^2} \\
                            \text{where} \quad \a &\in \{ \y, \r(\y) \} \\
                            \implies \frac{\p u}{\p\n} &= \frac{\x}{1} \cdot \frac{\x-\y}{\lvert\x-\y\rvert^2} + C\x\cdot\frac{\x-\r(\y)}{\lvert\x-\r(\y)\rvert^2}
                        \end{align*}
                        for $ x \in \pO $. Thus
                        \begin{align*}
                            1 &= g \\
                            &= \frac{\p u}{\p\n} \\
                            &= \x \cdot \frac{\x-\y}{\lvert\x-\y\rvert^2} + C\frac{\x\cdot(\x-\r)}{\lvert\x-\r\rvert^2}
                        \end{align*}
                        $\r(\y)$ is the inverse point of $\y$ in the circle, therefore
                        \begin{align*}
                            \frac{\lvert\x-\r(\y)\rvert}{\lvert\x-\y\rvert} &= \frac{1}{\y} \\
                            \implies 1 &= \frac{\lvert\x\rvert^2 - \x\cdot\y + C \left( \lvert\x\rvert^2 - \frac{\x\cdot\y}{\lvert\y\rvert^2} \right)(\y)^2}{\lvert\x-\y\rvert^2} \\
                            \implies \lvert\x-\y\rvert^2 &= \lvert\x\rvert^2 - (1+C)\x\cdot\y + C\lvert\x\rvert^2\lvert\y\rvert^2 \\
                            \implies C &= 1
                        \end{align*}
                \end{itemize}
        \end{enumerate}

    % Question 3
    \item
        \begin{enumerate}
            % Part A
            \item Define the \textbf{heat kernel} $K(\x,t)$ in $ \R^2 \times \R $
                \begin{itemize}
                    \item
                        \begin{equation*}
                            K(\x,t) =
                            \begin{dcases}
                                \frac{1}{4\pi t}e^{-\frac{\lvert\x\rvert^2}{4t}} \quad &t > 0 \\
                                0 &t \le 0
                            \end{dcases}
                        \end{equation*}
                \end{itemize}
            % Part B
            \item Show that
                $$ \frac{\p K}{\p t} = \Delta K \quad \text{in } \R^2 \times (0,\infty) $$
                and
                $$ \int_{\R^2} K(\x,t) \d\x = 1 \quad \forall t \in (0,\infty) $$
                \begin{itemize}
                    \item
                        \begin{align*}
                            \frac{\p K}{\p t} &= \left( -\frac{1}{4\pi t^2} + \frac{\lvert\x\rvert^2}{4\pi\cdot 4t^3} \right) e^{-\frac{\lvert\x\rvert^2}{4t}} \\
                            &= \left( \frac{\lvert\x\rvert^2}{4t^2} - \frac{1}{t} \right) K \\
                            \text{and} \quad \frac{\p K}{\p x_i} &= -\frac{2x_i}{4t}K \\
                            &= -\frac{x_i}{2t}K \\
                            \implies \frac{\p^2 K}{\p x_i^2} &= \left( \frac{x_i^2}{4t^2} - \frac{1}{2t} \right) K \\
                            \implies \D K &= \left( \frac{\lvert\x\rvert^2}{4t^2} - \frac{1}{t} \right) K \\
                            &= \frac{\p K}{\p t} \\
                            \text{and} \quad \int_{\R^2} K \d\x &= \frac{1}{4\pi t} \int_{\R^2} e^{-\frac{\lvert\x\rvert^2}{4t}} \d\x \\
                            &= \left( \frac{1}{\sqrt{4\pi t}} \int_{-\infty}^\infty e^{-\frac{\lvert\x\rvert^2}{4t}} \d\x \right)^2 \\
                            &= 1
                        \end{align*}
                \end{itemize}
            % Part C
            \item Consider the Cauchy problem
                $$ \frac{\p u}{\p t} = \frac{\p^2 u}{\p x_1^2} + \frac{\p^2 u}{\p x_2^2} \quad \text{in } \R^2 \times (0,\infty) $$
                $$ u \rvert_{t=0} = f $$
                with $f$ suitably continuous and bounded
                \begin{enumerate}[(i)]
                    \item Show that
                        $$ u(\x,t) = \int_{\R^2} K(\x-\y,t)f(\y) \d\y $$
                        satisfies the heat equation and
                        $$ \lim_{t \rightarrow 0^+} u(\x,t) = f(\x) $$
                        for all $ \x \in \R^2 $ at which $f$ is continuous
                \end{enumerate}
                \begin{itemize}
                    \item Consider
                        $$ \left( \frac{\p}{\p t} - \D\x \right) u(\x,t) = \int_{\R^2} \left( \frac{\p}{\p t} - \D\x \right) K(\x-\y,t)f(\y) \d\y = C $$
                        since
                        $$ \left( \frac{\p}{\p t} - \D\x \right) K(\x-\y,t) = 0 \quad \text{from (b)} $$
                        i.e. $u$ satisfies the heat equation. Also
                        \begin{align*}
                            u(\x,t) &= \int_{\R^2} K(\x-\y,t)f(\y) \d\y \\
                            &= \int_{\R^2} K(-\tilde{\z},t)f(\x+\tilde{\z})\d\tilde{\z} \\
                            &= \int_{\R^2} \frac{1}{t} K \left( \frac{\tilde{\z}}{t^{1/2}}, 1 \right) f(\x+\tilde{\z}) \d\tilde{\z} \\
                            &= \int_{\R^2} K(\z,1)f(\x+t^\frac{1}{2}\z) \d\z \\
                            \implies \lim_{t \rightarrow 0^+} u(\x,t) &= \int_{\R^2} K(\z,1)f(\x) \d\z \\
                            &= \left( \int_{\R^2} K(\z,1)\d\z \right) f(\x) \\
                            &= f(\x)
                        \end{align*}
                        using (b)
                \end{itemize}
                \begin{enumerate}[(i),resume]
                    \item Find the solution in the particular case
                        \begin{equation*}
                            f(x_1,x_2) =
                            \begin{cases}
                                x_1x_2 &\text{if } (x_1,x_2) \in (0,a) \times (0,b) \\
                                0 &\text{if } (x_1,x_2) \not\in (0,a) \times (0,b)
                            \end{cases}
                        \end{equation*}
                        What is the value of this solution at $ (x_1,x_2) = (a,b) $ when $t=0$?
                \end{enumerate}
                \begin{itemize}
                    \item
                        \begin{align*}
                            u(\x,t) &= \int_{y_1=0}^a \int_{y_2=0}^b y_1 \frac{1}{4\pi t} e^{-\frac{(x_1-y_1)^2}{4t}} e^{-\frac{(x_2-y_2)^2}{4t}} \d y_1 \d y_2 \\
                            &= \frac{1}{4\pi t} \int_{y_1=0}^a y_1 e^{-\frac{(x_1-y_1)^2}{4t}} \d y_1 \int_{y_2=0}^b e^{-\frac{(x_2-y_2)^2}{4t}} \d y_2 \\
                            &\ \ \vdots \\
                            &= \frac{1}{4\sqrt{\pi t}} \left[ 2t \left( 1-e^{-\frac{(a-x_1)^2}{4t}} \right) + x_1\sqrt{\pi t} \left\{ \text{erf}\left( \frac{a-x_1}{2\sqrt{t}} \right) + \text{erf}\left( \frac{x_1}{2\sqrt{t}} \right) \right\} \right] \\
                            &\qquad \cdot \left[ \text{erf}\left( \frac{b-x_2}{2\sqrt{t}} \right) + \text{erf}\left( \frac{x_2}{2\sqrt{t}} \right) \right]
                        \end{align*}
                        Hence, at $\x=(a,b)$
                        \begin{align*}
                            u(a,b,t) &= \frac{1}{4\pi t} \left[ a\sqrt{\pi t} \ \text{erf}\left( \frac{a}{2\sqrt{t}} \right) \right] \cdot \left[ \text{erf}\left( \frac{b}{2\sqrt{t}} \right) \right] \\
                            &= \frac{a}{4} \ \text{erf}\left( \frac{a}{2\sqrt{t}} \right) \text{erf}\left( \frac{b}{2\sqrt{t}} \right) \\
                            &\rightarrow \frac{a}{4} \text{ as } t \rightarrow 0^+
                        \end{align*}
                \end{itemize}
        \end{enumerate}

    % Question 4
    \item Let $ \O \subset \R^m $ be a bounded, open set with $\pO$ smooth enough that the divergence theorem can be applied. Define $\C$ by
        $$ \C := \{ u \in C^2(\Ob) : u = 0 \text{ on } \pO \} $$
        Given $ f \in C(\Ob) $, define the functional $ J(\cdot) $ by the Dirichlet integral
                $$ J(u) := \int_\O \left( \frac{1}{2} \lvert \nabla u(\x) \rvert^2 + f(\x)u(\x) \right) \d\x $$
        \begin{enumerate}
            % Part A
            \item Prove that if $ u \in \C $ is such that
                $$ J(u) \le J(v) \quad \forall v \in \C $$
                then $u$ staisfies
                \begin{align*}
                    \Delta u &= f \text{ in } \O \\
                    u &= 0 \text{ on } \pO
                \end{align*}
                (You may assume the \textbf{alternative vanishing lemma}, that if $ g \in C(\Ob) $ is such that
                $$ \int_\O g(\x)w(\x) \d\x = 0 \quad \forall w \in \C $$
                then $ g \equiv 0 $ in $\O$)
                \begin{itemize}
                    \item If $ u \in \C $ then $ u + \e\o \in \C $ for any $ \o \in \C $ and $ \e \in \R $, i.e. if $ u, \o \in C^2(\Ob) $ then $ u + \e\o \in C^2(\Ob) $ and
                        \begin{align*}
                            u + \e\o &= 0 \quad \text{on } \pO \\
                            \text{since} \quad u = \o &= 0 \quad \text{on } \pO
                        \end{align*}
                        Let
                        \begin{align*}
                            g(\e) &= J(u+\e\o) \\
                            &= \int_\O \frac{1}{2}(\nabla u + \e\nabla\o)\cdot(\nabla u + \e\nabla\o) + f(\x)(u+\e\o) \d\x \\
                            \implies \frac{\d g}{\d\e}(\e) &= \int_\O (\nabla u \cdot \nabla \o + \e \lvert \nabla\o \rvert^2 + f(\x)\o ) \d\x \\
                            \implies \frac{\d g}{\d\e}(0) &= \int_\O (\nabla u \cdot \nabla \o + f(\x) \o ) \d\x \\
                            &= -\int_\O \o\D u \d\x + \cancel{\int_\pO \o \frac{\d u}{\d\n} \d S} + \int_\O f(\x)\o \d\x \\
                            &= \int_\O (-\D u + f)\o \d\x
                        \end{align*}
                        For a minimum
                        \begin{align*}
                            g'(0) &= 0 \\
                            \implies \int_\O (-\D u + f)\o \d\x &= 0 \quad \forall \o \in \C
                        \end{align*}
                        By the Alternative Vanishing Theorem, since $ -\D u + f \in C(\Ob) $, we have
                        $$ \D u = f \quad \text{in } \O $$
                        \begin{flushright}
                            $\Box$
                        \end{flushright}
                \end{itemize}
            % Part B
            \item Prove that if $ u \in C^2(\Ob) $ satisfies
                \begin{align*}
                    \Delta u &= f \text{ in } \O \\
                    u &= 0 \text{ on } \pO
                \end{align*}
                then $ J(u) \le J(v) $ for all $ v \in \C $
                \begin{itemize}
                    \item For arbitrary $ v \in \C $, set $ v = u + w $. Then $ w \in \C $ and
                        \begin{align*}
                            J(v) &= J(u+w) \\
                            &= \int_\O \left( \frac{1}{2} (\nabla u + \nabla w ) \cdot ( \nabla u + \nabla w ) + f(u+w) \right) \d\x \\
                            &= \int_\O \frac{1}{2} \lvert \nabla u \rvert^2 + \frac{1}{2} \lvert \nabla w \rvert^2 + \nabla u \cdot \nabla w + uf + wf \d\x \\
                            \implies \int_\O \nabla u \cdot \nabla w + fw \d\x &= \int_\O \nabla u \cdot \nabla w + w \D u \d\x \\
                            &= \int_\pO w \frac{\p u}{\p\n} \d S \\
                            &= 0
                        \end{align*}
                        by Green's First Identity, with $ u,w \in C^2(\Ob) $ and using $ \D u = f $ in $\O$, $w=0$ on $\pO$. Therefore
                        \begin{align*}
                            J(v) &= J(u) + \int_\O \frac{1}{2} \lvert \nabla w \rvert^2 \d\x \\
                            &\ge J(u)
                        \end{align*}
                \end{itemize}
            % Part C
            \item Find the solution to $ u \in \C $ that minimises the above Dirichlet integral in the one-dimensional case $ m = 1, \O = [a,b] $ and $ f = 1 $
                \begin{itemize}
                    \item In 1D, $u$ satisfies
                        $$ \frac{\d^2 u}{\d x^2} = f(x) \quad a < x < b $$
                        with $u=0$ at $ x = a, b $

                        For $ f(x) = 1 $,
                        \begin{align*}
                            u(x) &= C_1x+C_2 + \frac{1}{2}x^2 \\
                            u(a) = 0 &\implies C_1a + C_2 = -\frac{a^2}{2} \\
                            u(b) = 0 &\implies C_1b+C_2 = -\frac{b^2}{2}
                        \end{align*}
                        Hence
                        \begin{align*}
                            C_1(a-b) &= -\frac{1}{2}a^2 + \frac{1}{2}b^2 \\
                            \implies C_1 &= -\frac{1}{2}(a+b) \\
                            \text{and} \quad C_2 &= \frac{1}{2}a(a+b)-\frac{a^2}{2} \\
                            &= \frac{1}{2}ab \\
                            \implies u(x) &= \frac{1}{2}x^2 - \frac{1}{2}(a+b)x + \frac{1}{2}ab \\
                            &= \frac{1}{2}(x-a)(x-b)
                        \end{align*}
                \end{itemize}
        \end{enumerate}

\end{enumerate}

\end{document}
