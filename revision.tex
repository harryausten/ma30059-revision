\documentclass{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{xcolor}
\usepackage{bm}

\numberwithin{equation}{section}

\newcommand  {\C}{\mathcal{C}}
\newcommand  {\F}{{\bm{F}}}
\newcommand  {\J}{\mathcal{J}}
\newcommand  {\R}{\mathbb{R}}
\renewcommand{\a}{{\bm{a}}}
\renewcommand{\b}{{\bm{b}}}
\renewcommand{\d}{\mathrm{d}}
\newcommand  {\n}{{\bm{n}}}
\renewcommand{\O}{\Omega}
\newcommand  {\Ob}{\bar{\Omega}}
\newcommand  {\p}{\partial}
\newcommand  {\pO}{{\partial\Omega}}
\renewcommand{\r}{{\bm{r}}}
\newcommand  {\x}{{\bm{x}}}
\newcommand  {\y}{{\bm{y}}}
\newcommand  {\z}{{\bm{z}}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{MA30059 Mathematical Methods II}}

    {\large \textbf{Revision}}
\end{center}

\section{Potential Equation}

\begin{itemize}
    \item \textbf{Divergence Theorem}

        Let $ \Omega \subset \R^m $ be a bounded, open set with sufficiently smooth boundary $\partial\Omega$. Let $\n(\x)$ denote the outward-pointing unit normal at $ \x \in \partial\Omega $. Let $ \F : \bar{\Omega} \rightarrow \R^m $ be in $ C'(\bar{\Omega}) $. Then
        $$ \int_\Omega \nabla \cdot \F(\x) \d\x = \int_{\partial\Omega} \F(\x) \cdot \n(\x) \d S(\x) $$

    \item \textbf{Green's Identities}

        Let $ \Omega \subset \R^m $ be a bounded, open set, sufficiently smooth for the divergence theorem to hold.
        \begin{enumerate}
            \item If $ u \in C'(\bar{\Omega}), v \in C^2(\bar{\Omega}) $ then
                $$ \int_\Omega (u\Delta v + \nabla u \cdot \nabla v ) \d\x = \int_{\partial\Omega} u \frac{\partial v}{\partial \n} \d S(\x) $$
            \item If $ u, v \in C^2(\bar{\Omega}) $ then
                $$ \int_\Omega ( u \Delta v - v \Delta u ) \d\x = \int_{\partial\Omega} ( u \frac{\partial v}{\partial\n} - v\frac{\partial u}{\partial\n} ) \d S(\x) $$
        \end{enumerate}

    \item \textbf{Vanishing Theorem}

        \begin{enumerate}
            \item Let $ f \in C[\a,\b] $ and $ f \ge 0 $ on $ [\a,\b] $

                $$ \int_a^b f(\x)\d\x = 0 \implies f(\x) = 0, \quad \forall \ \x \in [\a,\b] $$

            \item Let $ f \in C(\bar{\Omega}) $ and $ f \ge 0 $ on $\bar{\Omega}$

                $$ \int_\Omega f(\x) \d\x = 0 \implies f(\x) = 0, \quad \forall \ \x \in \bar{\Omega} $$
        \end{enumerate}

    \item \textbf{Newtonian Potential}

        \begin{equation*}
            N:\R^m \setminus \{ \bm{0} \} \rightarrow \R, \quad
            N(\x) =
            \begin{dcases}
                \frac{1}{w_2} \ln{\lvert \x \rvert}, & \text{if } m = 2, \\
                \frac{1}{(2-m)w_m} \lvert \x \rvert^{2-m}, & \text{if } m \ge 3, \\
            \end{dcases}
        \end{equation*}
        where $w_m$ is the surface area of the unit sphere $ \{ \x \in \R^m : \lvert \x \rvert = 1 \} $ (e.g. $w_2 = 2\pi$ and $w_3 = 4\pi$).

        $$ \nabla N(\x) = \frac{1}{w_m} \frac{\x}{\lvert\x\rvert^m} \quad \text{and} \quad \Delta N(\x) = 0, \quad \text{for } \x \in \R^m \setminus \{ \bm{0} \} $$
        $$ N_\a(\x) = N(\x - \a) = N(\a - \x) = N_\x(\a), \quad \text{for fixed } \a \in \R^m $$

    \item \textbf{Green's Integral Representation}

        Let $ \Omega \subset \R^m $ be a bounded domain with smooth boundary $\pO$ and let $ u \in C^1(\Ob) \cap C^2(\Omega) $ with $ \int_\Omega \lvert (\Delta u)(\y) \rvert \d^m \y < \infty $. Then, for any $\x \in \Omega$
        $$ u(\x) = \int_\Omega N_\x(\y)(\Delta u)(\y) \d^m\y + \int_\pO \left( u(\y)\frac{\p N_\x}{\p \n}(\y) - N_\x(\y)\frac{\p u}{\p \n}(\y) \right) \d S(\y) $$

    \item \textbf{Fundamental Solution of the Laplace Equation}

        The fundamental solution of the Laplace equation with respect to the point $ \a \in \R^m $ is a function/distribution $F$ satisfying
        $$ \Delta F(\x) = \delta(\x-\a) \quad, \x \in \R^m $$
        $N_\a$ is one such fundamental solution, i.e.
        $$ \Delta N_\a(\x) = \delta(\x-\a) \quad , \x \in \R^m $$

    \item \textbf{Mean Value Property}

        Let $ \Omega \subset \R^m $ be open and $ u \in C^2(\Omega) $ be harmonic. Let $ \x \in \Omega $ and $ \phi > 0 $ be such that $ \overline{B(\x,\phi)} \subset \Omega $. Then
        \begin{equation*}
            u(\x) = \frac{1}{w_m \phi^{m-1}} \int_{\lvert \y - \x \rvert = \phi} u(\y) \d S(\y)
        \end{equation*}

    \item \textbf{Strong Maximum Principle}

        Let $ \Omega \subset \R^m $ be open, bounded and connected, and let $ u \in C(\bar{\Omega}) \cap C^2(\Omega) $ be harmonic, i.e. $ \Delta u = 0 $ in $\Omega$.
        Then
        \begin{equation*}
            \text{either}
            \begin{cases}
                u \text{ is constant}, \\
                \text{or } u(\x) < \max_{\y \in \bar\Omega} u(\y), \quad \forall \ \x \in \Omega
            \end{cases}
        \end{equation*}

    \item \textbf{Weak Maximum Principle}

        Under the same conditions as above
        \begin{equation*}
            \max_{\y \in \bar\Omega} u(\y) = \max_{\y \in \partial\Omega} u(\y)
        \end{equation*}
        i.e. the max of a harmonic function occurs on the boundary

    \item \textbf{Dirichlet Problem}

        Let $ \Omega \subset \R^m $ be an open bounded set with boundary $\partial\Omega$. Given functions $f$ on $\Omega$ and $g$ on $\partial\Omega$, find a function $u$ on $\bar\Omega$ such that
        \begin{align*}
            \Delta u &= f \quad \text{in } \Omega \\
            u \vert_{\partial\Omega} &= g
        \end{align*}

    \item \textbf{Uniqueness}

        Suppose $u$ and $v$ are two solutions of the Dirichlet problem. Then $ w:= u - v $ satisfies the homogeneous Dirichlet problem
        \begin{align*}
            \Delta w &= 0 \quad \text{in } \Omega \\
            w \lvert_{\pO} &= 0
        \end{align*}
        Seeking a contradiction, suppose there exists $ \x_0 \in \O $ such that $ w(\x_0) \neq 0 $. Then by the strong maximum principle
        $$ w(\x_0) < 0 $$
        But we must also have
        \begin{align*}
            \Delta(-w) &= 0 \quad \text{in } \O \\
            -w \lvert_\pO &= 0
        \end{align*}
        therefore also by the strong maximum principle
        $$ -w(\x_0) < 0 $$
        which contradicts the previous statement
        \begin{flushright}
            $\Box$
        \end{flushright}

    \item \textbf{Green's Function}

        Let $ \Omega \subset \R^m $ be open. A Green's function for the Dirichlet problem is a function $ G(\x,\y) : \R^m \times \R^m \rightarrow \R $ defined as
        \begin{equation*}
            G(\x,\y) = N_\y(\x) + v(\x,\y) = N(\x - \y) + v(\x,\y)
        \end{equation*}
        where $v$ is such that for each fixed $ \y \in \Omega $
        \begin{itemize}
            \item $ v(\cdot,\y) \in C(\bar\Omega) \cap C^2(\Omega) $
            \item $ \Delta_\x v(\x,\y) = 0, \quad \forall \ \x \in \Omega $
            \item $ G(\x,\y) = 0, \quad \forall \ \x \in \partial\Omega $
        \end{itemize}
        Hence, Green's function is a solution to the problem
        \begin{align*}
            \Delta_\x G(\x,\y) &= \delta(\x-\y), \quad \x \in \Omega \\
            G(\x,\y) \vert_{\x \in \delta\Omega} &= 0
        \end{align*}
        where $ \y \in \Omega $ is a fixed point.

    \item \textbf{Symmetry}

        Fix arbitrary $ \x, \y \in \O $ such that $ \x \neq \y $. Let
        \begin{align*}
            u(\z) &:= G(\z,\y) \\
            \text{and} \quad v(\z) &:= G(\z,\x)
        \end{align*}
        Then we must have
        \begin{align*}
            (\Delta u)(\z) &= \delta(\z-\y) \\
            u \rvert_{\z \in \pO} &= 0
        \end{align*}
        and
        \begin{align*}
            (\Delta v)(\z) &= \delta(\z-\x) \\
            v \rvert_{\z \in \pO} &= 0
        \end{align*}
        Formal application of Green's second identity gives
        \begin{alignat*}{2}
            0 &= \int_\O ( u(\z)\delta(\z-\x) - v(\z)\delta(\z-\y) ) \d^m \z \\
            &= u(\x) - v(\y) \\
            &= G(\x,\y) - G(\y,\x)
        \end{alignat*}
        \begin{flushright}
            $\Box$
        \end{flushright}

    \item \textbf{Solution to Dirichlet Problem}

        Let $ \O \subset \R^m $ be open, bounded and connected with smooth boundary $\pO$, and let $ f \in C(\Ob) $ and $ g \in C(\pO) $. Let $u$ be the solution of the Dirichlet problem. If $ u \in C^1(\Ob) \cap C^2(\O) $, then
        $$ u(\x) = \int_\O G(\x,\y)f(\y) \d^m \y + \int_\pO \frac{\p G}{\p \n_\y}(\x,\y)g(\y) \d S(\y) \quad \forall \x \in \O $$

    \item \textbf{Explicit Green's Functions}

        \underline{Green's Function for the Disc}

        Let $ \O = B(0,\rho) \subset \R^2 $ for some $ \rho > 0 $. Fix an arbitrary $ \y \in \O $ and let
        $$ G(\x,\y) = \frac{1}{2\pi} \ln \lVert \x - \y \rVert + v_\y(\x) $$
        Then we need to find a solution to the problem
        \begin{alignat*}{2}
            \Delta v_\y(\x) &= 0 &&\x \in \O \\
            v_\y(\x) &= -\frac{1}{2\pi} \ln \lVert \x - \y \rVert \quad &&\x \in \pO
        \end{alignat*}
        If $ \y = \bm{0} $, we have the trivial solution given by $ v_{\bm{0}}(\x) = -\frac{1}{2\pi} \ln \rho $. If $ \y \neq \bm{0} $, then let
        $$ \r(\y) := \frac{\rho^2}{\lVert \y \rVert^2}\y $$
        which is called \textbf{inversion through the circle} because $ \lVert \y \rVert \lVert \r(\y) \rVert = \rho^2 $. Rearranging this, we obtain Green's function for the disc
        \begin{equation*}
            G(\x,\y) = \frac{1}{2\pi}
            \begin{dcases}
                \ln{\lVert \x \rVert} - \ln{\rho} &\text{if } \y = \bm{0} \\
                \ln{\lVert \x - \y \rVert} - \ln{\left( \frac{\lVert \y \rVert}{\rho} \lVert \x - \r(\y) \rVert \right)} &\text{if } \y \neq \bm{0}
            \end{dcases}
        \end{equation*}
        and the solution of the Dirichlet problem in a disc, known as \textbf{Poisson's formula for the disc}
        $$ u(\x) = \frac{\rho^2 - \lVert \x \rVert^2}{2\pi\rho} \int_{\lVert \y \rVert = \rho} \frac{g(\y)}{\lVert \x - \y \rVert^2} \d S(\y) $$

        \underline{Green's Function for the Ball}

        Let $ \O = B(0,\rho) \subset \R^3 $ for some $ \rho > 0 $. Arguing in the same manner as for the disc (inversion through the sphere), we obtain
        \begin{equation*}
            G(\x,\y) = -\frac{1}{4\pi}
            \begin{dcases}
                \lVert \y \rVert^{-1} - \rho^{-1} &\text{if } \x \neq \bm{0} \\
                \lVert \y - \x \rVert^{-1} - \left( \frac{\lVert \x \rVert}{\rho} \lVert \y - \r(\x) \rVert \right)^{-1} \quad &\text{if } \x \neq \bm{0}
            \end{dcases}
        \end{equation*}
        and \textbf{Possion's formula for the ball}
        $$ u(\x) = \frac{\rho^2 - \lVert \x \rVert^2}{4\pi\rho} \int_{\lVert \y \rVert = \rho} \frac{g(\y)}{\lVert \x - \y \rVert^3} \d S(\y) $$

        \underline{Green's Function for the Half-Plane}

        Let $ \O = \{ (x_1,x_2) \in \R^2 : x_2 > 0 \} $. Given a $ \y = (y_1,y_2) \in \O $, let $ \r(\y) = (y_1,-y_2) $ (reflection in the $x_1$-axis) and
        $$ G(\x,\y) = \frac{1}{2\pi}( \ln{\lVert \x - \y \rVert} - \ln{\lVert \x - \r(\y) \rVert} ) $$
        $$ u(\x) = \frac{x_2}{\pi} \int_{-\infty}^\infty \frac{g(y_1,0)}{\lVert \x - (y_1,0) \rVert^2} \d y_1 $$

        \underline{Green's Function for the Quarter-Plane}

        Let $ \O = \{ (x_1,x_2) \in \R^2 : x_1, x_2 > 0 \} $. Given a $ \y = (y_1,y_2) \in \O $, let $ r_1(\y) = (y_1, -y_2) $ (reflection about the $x_1$-axis), $ r_2(\y) = (-y_1,y_2) $ (reflection about the $x_2$-axis), and $ r_{12}(\y) = (-y_1,-y_2) $ (reflection about the $x_1$ and $x_2$ axes)
        $$ G(\x,\y) = \frac{1}{2\pi} ( \ln{\lVert \x - \y \rVert} - \ln{\lVert \x - \r_1(\y) \rVert} - \ln{\lVert \x - \r_2(\y) \rVert} + \ln{\lVert \x - \r_{12}(\y) \rVert} ) $$

    \item \textbf{Method of Images}

        In all of the above examples, the Green's functions are of the form
        $$ G(\x,\y) = N(\x-\y) + \sum_{i=1}^q \alpha_i(\y) N(\x - \r_i(\y)) + \beta(\y) $$
        where the term
        $$ v(\x,\y) = \sum_{i=1}^q \alpha_i(\y) N(\x-\r_i(\y)) + \beta(\y) $$
        depends on $\O$. The $\r_i(\y)$ are the \textbf{images} of $y$ obtained by reflections about lines and inversions through circles. The functions $\r_i(\y)$ and the coefficients $\alpha_i(\y)$ and $\beta(\y)$ have to be chosen such that for each $ \y \in \O $, $ \r_i(\y) \not\in \Ob $ and $ v(\x,\y) = -N(\x-\y) $, $ \forall \x \in \pO $

    \item \textbf{Neumann Problem}

        Given functions $f$ on $\Omega$ and $g$ on $\partial\Omega$, find a function $u$ on $\bar\Omega$ such that
        \begin{align*}
            \Delta u &= f \quad \text{in } \Omega \\
            \left. \frac{\partial u}{\partial\n} \right\vert_{\partial\Omega} &= g
        \end{align*}
        A Green's function for the Neumann Problem is defined by
        \begin{equation*}
            G(\x,\y) = N_\y(\x) + v(\x,\y) = N(\x-\y) + v(\x,\y)
        \end{equation*}
        where $v$ is such that for each fixed $ \y \in \Omega $
        \begin{enumerate}
            \item $ v(\cdot,\y) \in C^2(\bar\Omega) $ \quad (i.e. regular)
            \item $ \Delta_\x v(\x,\y) = 0, \quad \forall \ \x \in \Omega $ \quad (i.e. harmonic)
            \item $ \frac{\partial G}{\partial\n}(\x,\y) = \frac{1}{A}, \quad \forall \ \x \in \partial\Omega $, \quad where $A$ is the area of $\partial\Omega$
        \end{enumerate}
        Let $ \O \subset \R^m $ be open, bounded and connected, with smooth boundary $\pO$ and let $ f \in C(\Ob) $ and $ g \in C(\pO) $. Let $u$ be a solution of the Neumann problem. If $ u \in C^1(\Ob) \cap C^2(\O) $, then
        $$ u(\x) = \frac{1}{A} \int_\pO u(\y) \d S(\y) + \int_\O G(\x,\y) f(\y) \d^m \y - \int_\pO G(\x,\y)g(\y) \d S(\y) \quad \forall x \in \O $$

\end{itemize}

\section{Heat Equation}

\begin{itemize}

    \item \textbf{Heat Equation}

        Let $\Omega$ be an open set in $\R^m$ and $ t \in (0,T) $ for some $ T > 0 $ (possibly $ T = \infty $). Then $u(\x,t)$ satisfies the heat equation if
        $$ \frac{\partial u}{\partial t}(\x,t) = \Delta u(\x,t), \quad (\x,t) \in \Omega \times (0,T) $$
        We will consider
        \begin{itemize}
            \item The Cauchy problem ($ \Omega = \R^m $)
            \item The Cauchy-Dirichlet problem ($\Omega$ bounded)
        \end{itemize}

    \item \textbf{Heat Kernel}

        We define the heat kernel (or Gauss kernel) as
        $$ K : \R^m \times \R \rightarrow \R, \quad K(\x,t) =
        \begin{dcases}
            \frac{1}{(4\pi t)^\frac{m}{2}} e^{-\frac{\lvert \x \rvert^2}{4t}} &, t > 0 \\
            0 &, t \le 0
        \end{dcases}
        $$
        The heat kernel has the following properties
        \begin{alignat*}{2}
            \frac{\partial K}{\partial t}(\x,t) &= \Delta(\x,t) \quad &&, (\x,t) \in \R^m \times (0,\infty) \\
            \int_{\R^m} K(\x,t) \mathrm{d} \x &= 1 &&, t \in (0,\infty)
        \end{alignat*}
        The heat kernel is a fundamental solution of the heat equation, i.e.
        $$ \left( \frac{\p K}{\p t} - \Delta K \right)(\x,t) = \delta(\x)\delta(t) $$

    \item \textbf{Lemma}

        Let $ \varphi : \R^m \rightarrow \R $ be bounded and continuous at $\bm{0}$. Then
        $$ \lim_{t \rightarrow 0^+} \int_{\R^m} K(\x,t)\varphi(\x) \d^m \x = \varphi(\bm{0}) $$

    \item \textbf{Cauchy Problem}

        Given a function $f$ on $\R^m$, find a function $u$ on $ \R^m \times [0,\infty) $ such that
        \begin{alignat*}{2}
            \frac{\p u}{\p t} &= \Delta u \quad &&, (\x,t) \in \R^m \times (0,\infty) \\
            \left. u \right\vert_{t=0} &= f &&, \x \in \R^m
        \end{alignat*}
        An explicit representation for the solution to the Cauchy problem is given by
        $$ u(\x,t) = \int_{\R^m} K(\x-\y,t)f(\y) \d^m(\y) $$

    \item \textbf{Parabolic Boundary}

        Let $ \Omega \subset \R^m $ be open and bounded and let $T>0$. Consider the finite cylinder
        \begin{alignat*}{3}
            \p(\Omega \times (0,T)) &= \text{bottom} &&\cup \text{side} &&\cup \text{top} \\
            &= (\Omega \times \{0\}) &&\cup (\p\Omega \times [0,T]) &&\cup (\Omega \times \{T\})
        \end{alignat*}
        The set
        $$ \Gamma_T = (\Omega \times \{0\}) \cup (\p\Omega \times [0,T]) $$
        is called the parabolic boundary of the set $ \Omega \times (0,T) $ \par
        (i.e. the bottom and side but not the top)

    \item \textbf{Maximum Principle for the Heat Equation}

        Let $ \Omega \subset \R^m $ be open and bounded and let $T>0$. Suppose $ u \in C(\bar{\Omega} \times [0,T]) \cap C_1^2(\Omega \times (0,T)) $ satisfies the heat equation in $ \Omega \times (0,T) $. Then
        $$ \max_{\bar{\Omega} \times [0,T]} u(\x,t) = \max_{\Gamma_T} u(\x,t) $$

    \item \textbf{Cauchy-Dirichlet Problem}

        Let $ \Omega \subset \R^m $ be open and bounded and let $T>0$. Given the function $g$ on $\Gamma_T$, find a function $u$ on $ \bar{\Omega} \times [0,T] $ such that
        \begin{align*}
            \frac{\p u}{\p t} &= \Delta u \quad , (\x,t) \in \Omega \times (0,T) \\
            \left. u \right\vert_{\Gamma_T} &= g
        \end{align*}
        The second condition is shorthand for the initial condition and boundary condition:
        \begin{alignat*}{2}
            u(\x,0) &= f(\x) &&, \x \in \Omega \\
            u(\x,t) &= g(\x,t) \quad &&, (\x,t) \in \p\Omega \times [0,T]
        \end{alignat*}

    \item \textbf{Uniqueness}

        Suppose there are two solutions $u_1$ and $u_2$ and let $ v := u_2 - u_1 $. Then
        \begin{align*}
            \frac{\p v}{\p t} &= \Delta v \quad \text{in } \O \times (0,T) \\
            v \rvert_{\Gamma_T} &= 0
        \end{align*}
        hence, by the Maximum Principle for the Heat Equation
        $$ \max_{\Ob \times [0,T]} v(\x,t) = \max_{\Gamma_T} v(\x,t) = 0 $$
        therefore $ v \le 0 $ on $ \Ob \times [0,T] $, but the same argument applies to $-v$ and hence we conclude that $ v = u_2 - u_1 \equiv 0 $
        \begin{flushright}
            $\Box$
        \end{flushright}

    \item \textbf{Eigenvalues}

        A number $\lambda_k \in \R$ is said to be an \textbf{eigenvalue} of the Dirichlet Laplacian is there exists a function $w_k$, $w_k \not\equiv 0$, such that
        \begin{align*}
            -\Delta w_k &= \lambda_kw_k \quad \text{in } \O \\
            w_k \rvert_\pO &= 0
        \end{align*}

\end{itemize}

\section{Calculus of Variations}

\begin{itemize}
    \item \textbf{Setup}

        Let $ [a,b] \subset \R $ and $ F \in C^2([a,b] \times \R \times \R) $ where $ F = F(x,y,z) $. For a given $ \alpha, \beta \in \R $, let
        $$ \C = \{ u \in C^2[a,b] : u(a)=\alpha, u(b)=\beta \} $$
        Define the map (integral function) $ \J : \C \rightarrow \R $ by
        $$ \J(u) = \int_a^b F(x, u(x), u'(x)) \d x $$
        The main problem in Calculus of Variations is to find $ u \in \C $ such that
        \begin{equation} \label{min}
            \J(u) = \min_{v \in \C} \J(v)
        \end{equation}
        or
        \begin{equation}
            \J(u) = \max_{v \in \C} \J(v)
        \end{equation}
        Note: these are equivalent by changing the sign of $F$

    \item \textbf{Alternative Vanishing Lemma}

        Let
        $$ \C_0 := \{ u \in C^1[a,b] : u(a) = u(b) = 0 \} $$
        and $ f \in C[a,b] $. If
        $$ \int_a^b f(x)g'(x) \d x = 0 \quad \forall g \in \C_0 $$
        then $f$ is constant

        \underline{\textbf{Proof}}

        Set
        \begin{align*}
            c &:= \frac{1}{b-a} \int_a^b f(x) \d x \\
            \text{and} \quad h(x) &:= \int_a^x ( f(t) - c ) \d t
        \end{align*}
        Then $ h \in C^1[a,b] $ and $ h(a) = h(b) = 0 $, so $ h \in \C_0 $. Also, by the hypothesis of the lemma
        $$ \int_a^b f(x)h'(x) \d x = 0 $$
        Hence
        \begin{align*}
            \int_a^b ( f(x) - c ) h'(x) \d x &= \int_a^b f(x)h'(x) \d x - c(h(b)-h(a)) \\
            &= 0
        \end{align*}
        But $ h'(x) = f(x) - c $, so
        $$ \int_a^b ( f(x) - c )^2 \d x = 0 $$
        As the function $ f(x) - c $ is continuous on $[a,b]$ this implies
        $$ f(x) - c = 0 \quad \forall x \in [a,b] $$
        \begin{flushright}
            $\Box$
        \end{flushright}

    \item \textbf{Euler-Lagrange Equation}

        Let $F$ be defined as above and $ u \in C $ satisfy \eqref{min}. Then $ \forall x \in [a,b] $
        $$ \frac{\d}{\d x} \left( \frac{\partial F}{\partial z}(x,u(x),u'(x)) \right) - \frac{\partial F}{\partial y}(x,u(x),u'(x)) = 0 $$
        A function satisfying the above Euler-Lagrange equation is called an \textbf{extremal}
    \item \textbf{Special Cases}
        \begin{enumerate}
            \item If $F$ does not depend on $z$
                $$ \frac{\p F}{\p y}(x,u(x)) = 0 $$
            \item If $F$ does not depend on $y$
                \begin{alignat*}{2}
                    & \frac{\d}{\d x} \left( \frac{\p F}{\p z}(x,u'(x)) \right) &&= 0 \\
                    \Leftrightarrow \quad &\frac{\p F}{\p z}(x,u'(x)) &&= c_1
                \end{alignat*}
            \item If $F$ does not depend on $x$
                \begin{align*}
                    E(x) &:= F(u(x),u'(x)) - u'(x)\frac{\p F}{\p z}(u(x),u'(x)) \\
                    &= c_1 \ \text{constant}
                \end{align*}
                This is called \textbf{Beltrami's Identity}
        \end{enumerate}
    \item \textbf{Examples}
        \begin{enumerate}
            \item Geodesics on a cylinder

                Let $ \theta = x $, $ z(\theta) = u(x) $. Then $ F = \sqrt{1+\left(u'\right)^2} $ and
                $$ F(x,y,z) = F(z) = \sqrt{1-z^2} $$
                $F$ does not depend on $y$, therefore
                $$ \frac{\p F}{\p z}(u'(x)) = F'(u'(x)) = c_1 $$
                Differentiating $F$, we get
                $$ \frac{\p F}{\p z} = \frac{z}{\sqrt{1+z^2}} $$
                and hence
                $$ \frac{u'(x)}{\sqrt{1+\left(u'(x)\right)^2}} = c_1 $$
                Solving for $u'(x)$
                \begin{align*}
                    u'(x) &= \frac{c_1}{\sqrt{1-c_1^2}} \\
                    \implies u(x) &= \frac{c_1}{\sqrt{1-c_1^2}}x + c_2
                \end{align*}
                which represents a circular helix on the cylinder
            \item Brachistochrone

                Here $ y = u(x) $, $ z = u'(x) $ and
                $$ F(x,y,z) = F(y,z) = \sqrt{\frac{1+z^2}{y^2}} $$
                Hence, using Beltrami's Identity
                $$ x = \int \frac{\d u}{\sqrt{\frac{1}{c_1^2u} - 1}} + c_2 $$
                Now, make the substitutions
                $$ u = \frac{\tau}{c_1^2} \quad \text{and} \quad \tau = \sin^2 \frac{\theta}{2} $$
                giving
                \begin{align*}
                    x - c_2 &= \frac{1}{c_2} \int \sqrt{\frac{\tau}{1-\tau}} \ \d \tau \\
                    &= \frac{1}{2c_1^2} \int (1-\cos\theta) \ \d \theta \\
                    &= \frac{1}{2c_1^2} (\theta - \sin\theta)
                \end{align*}
                which is a parametric representation of a cycloid
        \end{enumerate}
\end{itemize}

\end{document}
