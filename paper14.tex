\documentclass[11pt,a4paper]{article}

\usepackage[margin=1cm]{geometry}
\usepackage[shortlabels]{enumitem}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{bm}
\usepackage{mathtools}

\newcommand  {\C}{\mathcal{C}}
\renewcommand{\d}{{\ \mathrm{d}}}
\newcommand  {\du}[1]{{\underline{\underline{#1}}}}
\newcommand  {\D}{\Delta}
\newcommand  {\e}{\epsilon}
\renewcommand{\O}{\Omega}
\newcommand  {\Ob}{{\bar{\Omega}}}
\newcommand  {\p}{\partial}
\newcommand  {\pO}{{\partial\Omega}}
\renewcommand{\r}{{\bm r}}
\newcommand  {\R}{\mathbb{R}}
\newcommand  {\x}{{\bm x}}
\newcommand  {\y}{{\bm y}}
\newcommand  {\n}{{\bm n}}
\renewcommand{\u}{{\bm u}}
\newcommand  {\z}{{\bm z}}
\newcommand  {\zt}{\tilde{\bm z}}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{MA30059 Mathematical Methods II}}

    {\large \textbf{2014 Paper}}
\end{center}

\begin{enumerate}

    % Question 1
    \item Let $\Omega \subset \mathbb{R}^3$ be a bounded open set with sufficiently smooth boundary $\partial\Omega$ for application of the divergence theorem.

        % Part A
        \begin{enumerate}
            \item Consider the Dirichlet problem defined as: given $ f \in C(\bar{\Omega}), g \in C(\partial\Omega) $, find $ u \in C^2(\bar{\Omega}) $ such that
                \begin{align*}
                    \Delta u = f \ \operatorname{in} \ \Omega, \\
                    u = g \ \operatorname{on} \ \partial\Omega.
                \end{align*}
                Using Green's first identity, or otherwise, show that there is at most one solution to the Dirichlet problem.
        \end{enumerate}

        % Answer
        \begin{itemize}
            \item Let $ u_1, u_2 \in C^2(\bar{\Omega}) $ satisfy the stated Dirichlet problem. Then $ v = u_1 - u_2 \in C^2(\bar{\Omega}) $ satisfies
                \begin{align*}
                    \Delta v &= 0 \ \operatorname{in} \ \Omega \quad \operatorname{and} \quad v = 0 \ \operatorname{on} \ \partial \Omega \\
                    \implies 0 &= \int_{\Omega} v \Delta v dx \\
                    \implies 0 &= \int_{\partial\Omega} v \frac{\partial v}{\partial n} dS(x) - \int_{\Omega} \lvert \nabla v \rvert^2 dx \\
                    \implies 0 &= \int_{\Omega} \lvert \nabla v \rvert^2 dx \quad \operatorname{since} \quad v \rvert_{\partial\Omega} = 0 ,
                \end{align*}
                where we have used Green's first identity.

                But $ \lvert \nabla v \rvert^2 \in C(\bar{\Omega}) $ and $ \lvert \nabla v \rvert^2 \ge 0 $, then by the Vanishing Theorem (higher dimension version to the formula sheet), $ \nabla v = 0 \quad \forall x \in \bar{\Omega} $.

                Hence $ v = \operatorname{constant} = 0 \quad \forall x \in \bar{\Omega} $ using $ v \rvert_{\partial\Omega} = 0 $.

                Hence $ u_1 = u_2 \forall x \in \bar{\Omega} $ and there is at most one solution.
        \end{itemize}

        % Part B
        \begin{enumerate}[resume]
            \item Let $ \Omega = B(x,a) $ be the ball of radius $ a > 0 $ with centre $x$ and $ u \in C^1(\bar{\Omega}) \cap C^2(\Omega) $ be harmonic in $\Omega$. Consider
                $$ H(x,y) = -\frac{1}{4\pi \lvert y-x \rvert} + \frac{1}{4\pi a}, $$
                for $ y \in \bar{\Omega} $.

                % Part I
                \begin{enumerate}[(i)]
                    \item For $ y \in \partial B(x,a) $, show that
                        $$ H(x,y) = 0 \ \operatorname{and} \ \frac{\partial H}{\partial n}(x,y) = \frac{1}{4\pi a^2}, $$
                        where $ \partial / \partial n $ denotes the derivative in the usual outward unit normal direction.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item For $ y \in \partial B(x,a) $, $ \lvert y - x \rvert = a \Rightarrow H(x,y) = 0 $.
                        \begin{align*}
                            \frac{\partial H}{\partial n}(x,y) &= n(y) \cdot \nabla_y H(x,y), \quad \operatorname{where} \ n(y) = \frac{y-x}{\lvert y-x \rvert} = \frac{y-x}{a} \\
                            \implies \nabla_y H(x,y) &= \nabla_y (-\frac{1}{4\pi} \lvert y-x \rvert^{-1} ) = \frac{1}{4\pi} \lvert y-x \rvert^{-2} \frac{y-x}{\lvert y-x \rvert} \\
                            \therefore \frac{\partial H}{\partial n}(x,y) &= \frac{1}{4\pi a^2}
                        \end{align*}
                \end{itemize}

                % Part II
                \begin{enumerate}[(i),resume]
                    \item Let $ \Omega_\epsilon = B(x,a) \setminus \overline{B(x,\epsilon)} $ with $ 0 < \epsilon < a $. By applying Green's second identity to $ u(y) $ and $ H(x,y) $ in $ \Omega_\epsilon $, show that in the limit $ \epsilon \rightarrow 0 $ we obtain the Mean Value Property:
                        $$ u(x) = \frac{1}{4 \pi a^2} \int_{\lvert y - x \rvert = a} u(y)dS(y) .$$
                        (You may assume that $ \Delta H = 0 $ for $ y \in \mathbb{R}^3 \setminus \{ x \} $.)
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item We have $ \Omega_\epsilon = B(\x,a) \setminus \overline{B(\x,\epsilon)} $, $ H \in C^2(\bar{\Omega}_\epsilon) $ and $ \u \in C'(\bar{\Omega}) \cap C^2(\Omega) $, therefore Green's second identity gives
                        \begin{align*}
                            \int_{\Omega_\epsilon} \left( H(\x,\y) \Delta \u(\y) - \u(\y) \Delta_\y H(\x,\y) \right) d\y &= \int_{\partial B(\x,a)} \left( H(\x,\y) \frac{\partial\u}{\partial\n}(\y) - \u(\y) \frac{\partial H}{\partial\n}(\x,\y) \right) dS \\
                            &+ \int_{\partial B(\x,\epsilon )} \left( H(\x,\y) \frac{\partial\u}{\partial\n}(\y) - \u(\y) \frac{\partial H}{\partial\n}(\x,\y) \right) dS(\y)
                        \end{align*}
                        Now $ \Delta\u = 0 $, $ \Delta H = 0 $, in $\Omega_\epsilon$ and hence
                        \begin{equation} \label{bii}
                            \int_{\partial B(\x,a)} \u(\y) \frac{1}{4\pi a^2} dS(\y) = \int_{\partial B(\x,\epsilon)} \left( H(\x,\y) \frac{\partial\u}{\partial\n}(\y) - \u(\y) \frac{\partial H}{\partial\n}(\x,\y) \right) dS(\y)
                        \end{equation}
                        using the results in (i) for $H$ and $\frac{\partial H}{\partial\n}$ for $ y \in \partial B(\x,a) $.
                        \begin{align*}
                            \left\vert \int_{\partial B(\x,\epsilon)} H(\x,\y) \frac{\partial\u}{\partial\n}(\y) dS(\y) \right\vert &\le \max_{\y \in \partial B(\x,\epsilon)} \lvert H(\x,\y) \rvert \int_{\partial B(\x,\epsilon)} \left\vert \frac{\partial\u}{\partial\n} \right\vert dS \\
                            &\le \left\vert \frac{1}{4\pi a} - \frac{1}{4\pi \epsilon} \right\vert M4\pi \epsilon^2 \rightarrow 0 \text{ as } \epsilon \rightarrow 0
                        \end{align*}
                        where $ \left\vert \frac{\partial\u}{\partial\n} \right\vert \le \lvert \nabla \u \rvert \le M $ constant since $ \u \in C^2(\bar{\Omega}) $.
                        \begin{align*}
                            \int_{\partial B(\x,\epsilon)} \u(\y) \frac{\partial H}{\partial\n}(\x,\y) dS(\y) &= \int_{\partial B(\x,\epsilon)} \u(\y) \left( -\frac{1}{4\pi \epsilon^2} \right) dS(\y) \\
                            &= -\frac{1}{4\pi\epsilon^2} \int_{\partial B(\x,\epsilon)} \left( \u(\x) + 0(\epsilon) \right) dS(\y) \\
                            &= -\u(\x) \text{ as } \epsilon \rightarrow 0
                        \end{align*}
                        As $ \epsilon \rightarrow 0 $, \eqref{bii} gives the Mean Value Property
                        \begin{equation*}
                            \u(\x) = \frac{1}{4\pi a^2} \int_{\partial B(\x,a)} \u(\y) dS(\y) = \frac{1}{4\pi a^2} \int_{\lvert \y - \x \rvert = a} \u(\y) dS(\y)
                        \end{equation*}
                \end{itemize}

                % Part III
                \begin{enumerate}[(i),resume]
                    \item Let $ y = (y_1,y_2,y_3) $ and
                        $$ u(y_1,y_2,y_3) = y_1^2 + y_2^2 + y_3^2 - 2y_1 + 2 \quad \operatorname{for} \quad y \in \partial B(x,2) ,$$
                        with $ x = (1,0,0) $. Determine the value of $u$ at the point $ (1,0,0) $.
                        (If needed, in spherical polar coordinates
                        $$ y - x = ( a \sin{\theta} \cos{\phi}, a \sin{\theta} \sin{\phi}, a \cos{\theta} ) $$
                        with $ 0 \le \theta \le \pi, 0 \le \phi \le 2 \pi $ when $ \lvert y - x \rvert = a $. Also $ dS(y) = a^2 \sin{\theta} d \theta d \phi $.)
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item
                        \begin{align*}
                            u(\y) &= (y_1-1)^2 + y_2^2 + y_3^2 + 1 \\
                            &= \lvert \y-\x\rvert^2 + 1 \quad \forall \y \in \p B(\x,2) \\
                            \text{and} \quad \x &= (1,0,0)
                        \end{align*}
                        By part (ii) with $ a = 2 $
                        \begin{align*}
                            u(1,0,0) &= \frac{1}{16\pi} \int_{\lvert\y-\x\rvert=2} \lvert\y-\x\rvert^2+1 \d S(\y) \\
                            &= \frac{1}{16\pi} \cdot 5 \cdot 4\pi \cdot 4 \\
                            &= 5
                        \end{align*}
                \end{itemize}
        \end{enumerate}

    % Question 2
    \item Let $ \Omega \subset \mathbb{R}^2 $ be non-empty and open, and consider the Dirichlet problem:
        \begin{align*}
            \Delta u = 0 \ \operatorname{in} \ \Omega , \\
            u \rvert_{\partial\Omega} = g.
        \end{align*}

        \begin{enumerate}
            % Part A
            \item

                % Part I
                \begin{enumerate}[(i)]
                    \item Define the shifted Newtonian potential $ N(x - y) $ for fixed $ y \in \mathbb{R}^2 $. Show that
                        $$ \nabla N = \frac{1}{2\pi} \frac{x - y}{\lvert x - y \rvert^2} \ \operatorname{and} \ \Delta N = 0 \ \operatorname{for} \ x \in \mathbb{R}^2 \setminus \{ y \} .$$
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item The shifted Newtonian potential on $ \R^2 \setminus \{\y\} $ is defined as
                        \begin{align*}
                            N_\y(\x) &= N(\x-\y) \\
                            &= \frac{1}{2\pi} \ln{\lvert\x-\y\rvert}
                        \end{align*}
                        Using this definition, we can calculate
                        \begin{align*}
                            \nabla N &= \frac{1}{2\pi}\frac{1}{\lvert\y-\x\rvert}\frac{\x-\y}{\lvert\x-\y\rvert} \\
                            &= \frac{\x-\y}{2\pi\lvert\x-\y\rvert^2} \\
                            \implies \D N &= \nabla \cdot (\nabla N) \\
                            &= \frac{1}{2\pi}\nabla \cdot \left[ (\x-\y)\lvert\x-\y\rvert^{-2} \right] \\
                            &= \frac{1}{2\pi} \left[ 2\lvert\x-\y\rvert^{-2} -2(\x-\y)\cdot\frac{\x-\y}{\lvert\x-\y\rvert}\lvert\x-\y\rvert^{-3} \right] \\
                            &= 0 \quad \forall \x \neq \y
                        \end{align*}
                        \begin{flushright}
                            $\Box$
                        \end{flushright}
                \end{itemize}

                % Part II
                \begin{enumerate}[(i),resume]
                    \item Define the notion of a Green's function $ G(x,y) $ for the above Dirichlet problem in the domain $\Omega$.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item A Green's function for the Dirichlet problem is a function $ G(\x,\y) : \R^m \times \R^m \rightarrow \R $ defined as
                        \begin{align*}
                            G(\x,\y) &= N_\y(\x) + v(\x,\y) \\
                            &= N(\x-\y) + v(\x,\y)
                        \end{align*}
                        where $v$ is such that for each fixed $ \y \in \O $
                        \begin{itemize}
                            \item $ v(\cdot,\y) \in C(\Ob) \cap C^2(\O) $
                            \item $ \D_\x v(\x,\y) = 0 \quad \forall \x \in \O $
                            \item $ G(\x,\y) = 0 \quad \forall \x \in \pO $
                        \end{itemize}
                \end{itemize}

                % Part III
                \begin{enumerate}[(i),resume]
                    \item Starting from Green's integral representation, show that
                        $$ u(x) = \int_{\partial\Omega} \frac{\partial G}{\partial n}(x,y) g(y) \d S(y), \ \forall x \in \Omega .$$
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item For $ u \in C^1(\Ob) \cap C^2(\O) $, Green's integral representation is
                        $$ u(\x) = \int_\O N_\x(\y) \D u(\y) \d^m\y + \int_\pO \left( u(\y)\frac{\p N_\x}{\p\n}(\y) - N_\x(\y)\frac{\p u}{\p\n}(\y) \right) \d S(\y) $$
                        where
                        $$ N_\x(\y) = G(\x-\y) - v(\x,\y) $$
                        Hence
                        \begin{equation} \tag{*} \label{u}
                            u(\x) = \int_\pO \left( u(\y) \frac{\p}{\p\n}G(\x,\y) - u(\y)\frac{\p v}{\p\n}(\x,\y) + v(\x,\y)\frac{\p u}{\p\n}(\x,\y) - G(\x,\y)\frac{\p u}{\p\n}(\y) \right) \d S(\y)
                        \end{equation}
                        Now $ G(\x,\y) = G(\y,\x) $ and so $ v(\x,\y) = v(\y,\x) $. Thus
                        $$ G(\x,\y) \rvert_{\y \in \pO} = G(\y,\x) \rvert_{\y \in \pO} = G(\x,\y) \rvert_{\x \in \pO} = 0 $$
                        Also, by (ii)
                        \begin{align*}
                            \D_\y v(\x,\y) &= \D_\y v(\y,\x) \\
                            &= \D_\x v(\x,\y) \\
                            &= 0 \\
                            \implies \int_\pO &\left( v(\x,\y)\frac{\p u}{\p\n}(\y) - u(\y)\frac{\p v}{\p\n}(\x,\y) \right) \d S(\y) \\
                            &= \int_\O \left( v(\x,\y) \cancelto{0}{\D_\y u(\y)} - u(\y) \cancelto{0}{\D_\y v(\x,\y)} \right) \d S(\y) \\
                            &= 0
                        \end{align*}
                        Thus \eqref{u} reduces to
                        $$ u(\x) = \int_\pO g(\y) \frac{\p}{\p\n} G(\x,\y) \d S(\y) $$
                        using $ u\rvert_\pO = g $
                \end{itemize}

            % Part B
            \item Now let $ \Omega  = \{ x \in \mathbb{R}^2 : x_2 > 0 \} $, with additionally $ u(x) \rightarrow 0 $ as $ \lvert x \rvert \rightarrow \infty $ and $g$ bounded. For given $ y = (y_1,y_2) \in \Omega $, consider
                \begin{equation} \label{greens}
                    G(x,y) = N(x - y) - N(x - r(y)) ,
                \end{equation}
                for some $ r(y) \notin \bar{\Omega} $.

                % Part I
                \begin{enumerate}[(i)]
                    \item By identifying a suitable $ r(y) $, show that the function in \eqref{greens} is a Green's function for the Dirichlet problem.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item If $ \y \in \O $ then $ \r(\y) \not\in \Ob \implies G(\x,\y) \in C^2(\Ob) $
                    \item $ \D_\x G(\x,\y) = \D_\x N(\x-\y) - \D_\x N(\x-\r(\y)) = 0 $ by (a)(i)
                    \item For $ \x \in \pO $, $ \lvert\x-\y\rvert = \lvert\x-\r(\y)\rvert \implies G(\x,\y) = 0 $
                \end{itemize}

                % Part II
                \begin{enumerate}[(i),resume]
                    \item Hence show that this Green's function is symmetric, i.e. $ G(x,y) = G(y,x) $ for the identified $ r(y) $.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item
                        \begin{align*}
                            N(\x-\y) &= N(\y-\x) \\
                            \text{and} \quad 2\pi N(\y-\r(\x)) &= \ln{\lvert\y-\r(\x)\rvert} \\
                            &= \frac{1}{2}\ln{\left( (y_1-x_1)^2 + (y_2+x_2)^2 \right)} \\
                            &= \ln{\lvert\x-\r(\y)\rvert} \\
                            &= 2\pi N(\x-\r(\y)) \\
                            \text{hence} \quad G(\x,\y) &= G(\y,\x)
                        \end{align*}
                \end{itemize}

                % Part III
                \begin{enumerate}[(i),resume]
                    \item Assuming that the expression in (a)(iii) holds, find the explicit solution $ u(x) $ to the Dirichlet problem when
                        \begin{equation*}
                            u(x_1,0) =
                            \begin{cases}
                                1 \quad \operatorname{if} 0 \le x_1 \le 1, \\
                                0 \quad \operatorname{otherwise}.
                            \end{cases}
                        \end{equation*}
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item
                        \begin{align*}
                            u(\x) &= \int_{y_1=0}^1 \left. -\frac{\p G}{\p y_2}(\x,\y) \right\vert_{y_2=0} \d y_1 \\
                            \text{and} \quad G(\x,\y) &= \frac{1}{2\pi}\frac{1}{2} \ln{ \left( (x_1-y_1)^2 + (x_2-y_2)^2 \right) } - \frac{1}{4\pi} \ln{ \left( (x_1-y_1)^2 + (x_2+y_2)^2 \right) } \\
                            \implies \frac{\p G}{\p y_2} &= -\frac{1}{2\pi} \left( \frac{x_2-y_2}{(x_1-y_1)^2+(x_2-y_2)^2} - \frac{x_2+y_2}{(x_1-y_1)^2 + (x_2+y_2)^2} \right) \\
                            \therefore u(\x) &= \frac{1}{\pi} \int_{y_1=0}^1 \frac{x_2}{(x_1-y_1)^2+x_2^2} \d y_1 \\
                            &= \frac{1}{\pi} \int_{\tan^{-1}{-\frac{x_1}{x_2}}}^{\tan^{-1}{\frac{1-x_1}{x_2}}} \frac{x_2^2}{x_2^2} \d \theta \\
                            &= \frac{1}{\pi} \left( \tan^{-1}{ \left( \frac{1-x_1}{x_2} \right) } - \tan^{-1}{ \left( -\frac{x_1}{x_2} \right) } \right)
                        \end{align*}
                \end{itemize}
        \end{enumerate}

    % Question 3
    \item

        % Part A
        \begin{enumerate}
            \item Let $ f \in C(\mathbb{R}^2) $ be bounded. Consider the Cauchy problem
                $$ \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2} \ \operatorname{in} \ \mathbb{R}^2 \times (0,\infty) ,$$
                $$ u \rvert_{t=0} = f .$$
                Show that
                $$ u(x,t) = \int_{\mathbb{R}^2} K(x - y, t) f(y) dy $$
                satisfies the heat equation and
                $$ \lim_{t \rightarrow 0^+} u(x,t) = f(x) ,$$
                for all $ x \in \mathbb{R}^2 $. (Here $ K(x,t) $ denotes the heat kernel, any properties of which should be shown and not simply quoted.)
        \end{enumerate}

        % Answer
        \begin{itemize}
            \item The \textbf{heat kernel} is defined as
                \begin{equation*}
                    K(\x-\y,t) =
                    \begin{dcases}
                        \frac{1}{4\pi t} e^{-\frac{\lvert\x-\y\rvert^2}{4t}} &t > 0 \\
                        0 &t \le 0
                    \end{dcases}
                \end{equation*}
                Hence
                \begin{align*}
                    \frac{\p K}{\p t} &= -\frac{1}{4\pi t^2} e^{-\frac{\x-\y\rvert^2}{4t}} + \frac{\lvert\x-\y\rvert^2}{4t^2} \frac{1}{4\pi t} e^{-\frac{\lvert\x-\y\rvert^2}{4t}} \\
                    &= \left( \frac{\lvert\x-\y\rvert^2}{4t^2} - \frac{1}{t} \right) K(\x-\y,t) \\
                    \text{and} \quad \frac{\p K}{\p x_i} &= \frac{1}{4\pi t} \left( -\frac{x_i-y_i}{2t} \right) e^{-\frac{\lvert x_i - y_i \rvert^2}{4t}} \\
                    &= -\frac{x_i-y_i}{2t} K \\
                    \implies \frac{\p^2 K}{\p x_i^2} &= -\frac{K}{2t} + \frac{(x_i-y_i)^2}{4t^2} K \\
                    &= \left( \frac{(x_i-y_i)^2}{4t^2} - \frac{1}{2t} \right) K \\
                    \implies \D_\x K &= \left( \frac{\lvert\x-\y\rvert^2}{4t^2} - \frac{1}{t} \right) K(\x-\y,t) \\
                    &= \frac{\p}{\p t}K(\x-\y,t)
                \end{align*}
                i.e. we have $ \left( \displaystyle\frac{\p}{\p t} - \D_\x \right) K = 0 $. Hence $u$ satisfies the heat equation since
                $$ \left( \frac{\p}{\p t} - \D_\x \right) u(\x,t) = \int_{\R^2} \left( \frac{\p}{\p t} - \D_\x \right) K(\x-\y,t)f(\y) \d\y = 0 $$
                Now, let $ \zt = \y - \x $, then
                \begin{align*}
                    u(\x,t) &= \int_{\R^2} K(\x-\y,t)f(\y) \d\y \\
                    &= \int_{\R^2} K(-\zt,t)f(\x+\zt) \d\zt \\
                    &= \int_{\R^2} \frac{1}{t} K \left( \frac{\zt}{t^{1/2}}, 1 \right) f(\x+\zt) \d\zt \\
                    &= \int_{\R^2} K(\z,1)f(\x+t^{1/2}\z)\d\z
                \end{align*}
                where $ \z = t^{1/2}\zt $. Therefore
                \begin{align*}
                    \int_{\R^2} K(\z,1) \d\z &= \int_{z_1=-\infty}^\infty \int_{z_2=-\infty}^\infty \frac{1}{4\pi} e^{-z_1^2-z_2^2} \d z_1 \d z_2 \\
                    &= \left( \frac{1}{2\pi}  \int_{z_1=-\infty}^\infty e^{-z_1^2} \d z_1 \right)^2 \\
                    &= 1
                \end{align*}
        \end{itemize}

        \begin{enumerate}[resume]
            % Part B
            \item Let $ \Omega \subset \mathbb{R}^m $ be open, bounded and connected, and let $ T > 0 $. Consider the heat equation:
                \begin{equation} \label{heat}
                    \frac{\partial u}{\partial t}(x,t) = (\Delta u)(x,t), \quad \forall x \in \Omega, \quad t \in (0,T).
                \end{equation}

                % Part I
                \begin{enumerate}[(i)]
                    \item Define the parabolic boundary $ \Gamma_T $ of the set $ \Omega \times (0,T) $.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item The \textbf{parabolic boundary} of the set $ \O \times (0,T) $ is defined as
                        $$ \Gamma_T = (\O \times \{0\}) \cup (\pO \times [0,T]) $$
                \end{itemize}

                % Part II
                \begin{enumerate}[(i),resume]
                    \item State, without proof, the maximum principle for the heat equation.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item Suppose
                        $$ u \in C(\Ob \times [0,T]) \cap C_1^2(\O \times (0,T)) $$
                        satisfies the heat equation in $ \O \times (0,T) $. Then
                        $$ \max_{\Ob \times [0,T]} u(\x,t) = \max_{\Gamma_T} u(\x,t) $$
                \end{itemize}

                % Part III
                \begin{enumerate}[(i),resume]
                    \item Let $ g \in C(\Gamma_T) $. Show that there exists at most one solution $ u \in C(\bar{\Omega} \times [0,T]) \ \cap \ C_1^2(\Omega \times (0,T)) $ of \eqref{heat} satisfying $ u \rvert_{\Gamma_T} = g $.
                \end{enumerate}

                % Answer
                \begin{itemize}
                    \item Let $ u_1, u_2 $ be two solutions. Then $ v = u_1 - u_2 $ satisfies
                        $$ \frac{\p v}{\p t} = \D v \quad \text{in} \quad \O \times (0,T) \quad \text{with} \quad v \rvert_{\Gamma_T} = 0 $$
                        By the Maximum Principle
                        \begin{align*}
                            \max_{\Ob \times [0,T]} v(\x,t) &= \max_{\Gamma_T} v(\x,t) \\
                            &= 0 \\
                            \implies v &\le 0 \quad \text{on} \quad \Ob \times [0,T]
                        \end{align*}
                        Similarly, for $-v$
                        \begin{align*}
                            -v &\le 0 \quad \text{on} \quad \Ob \times [0,T] \\
                            \implies v = 0 \quad \text{on} \quad \Ob \times [0,T]
                        \end{align*}
                \end{itemize}

            % Part C
            \item Let $ \Omega = \{ x \in \mathbb{R}^2 : \lvert x \rvert < 1 \} $. Suppose that $ u \in C(\bar{\Omega} \times [0,\infty)) \ \cap \ C_1^2(\Omega \times (0,\infty)) $ satisfies
                $$ \frac{\partial u}{\partial t}(x,t) = (\Delta u)(x,t) + \lvert x \rvert^2, \quad \forall x \in \Omega, \quad t > 0 ,$$
                $$ u(x,0) = 0, \quad \forall x \in \Omega ,$$
                $$ u(x,t) = 0, \quad \forall x \in \partial\Omega, \quad t \ge 0 .$$
                Prove that
                $$ u(x,t) \le v(x), \quad \forall x \in \bar{\Omega}, \quad t \ge 0 ,$$
                where $ v(x) = \frac{1}{16}(1 - \lvert x \rvert^4) $.
        \end{enumerate}

        % Answer
        \begin{itemize}
            \item
                \begin{align*}
                    v &= \frac{1}{16}\left( 1-\lvert\x\rvert^4 \right) \\
                    &= \frac{1}{16} \left( 1 - (x_1^2+x_2^2)^2 \right) \\
                    \implies \frac{\p v}{\p x_1} &= -\frac{1}{4}x_1(x_1^2+x_2^2) \\
                    \text{and} \quad \frac{\p^2 v}{\p x_1^2} &= -\frac{1}{4}(x_1^2+x_2^2) - \frac{1}{2}x_1^2 \\
                    \therefore \D v &= \frac{\p^2 v}{\p x_1^2} + \frac{\p^2 v}{\p x_2^2} \\
                    &= -\frac{1}{2}\lvert\x\rvert^2 - \frac{1}{2}\lvert\x\rvert^2 \\
                    &= -\lvert\x\rvert^2
                \end{align*}
                Let $ u(\x,t) = v(\x) + w(\x,t) $. Then
                \begin{align*}
                    \frac{\p w}{\p t} &= \D w \quad \text{in} \quad \O \times (0,\infty) \\
                    \text{and} \quad w(\x,t) &= -v(\x) \quad \x \in \O \\
                    \implies w(\x,t) &= 0 \quad \forall \x \in \pO
                \end{align*}
                since $ v(\x) = 0 $, $ \forall \x \in \pO $. Then, by the Maximum Principle
                \begin{align*}
                    w(\x,t) &\le \max_{\Gamma_T} w(\x,t) = 0 \\
                    \implies u(\x,t) &\le v(\x) \quad \forall \x \in \Ob, t \ge 0
                \end{align*}
        \end{itemize}

        % Question 4
        \item

            \begin{enumerate}
                % Part A
                \item Let $ [a,b] \subset \mathbb{R}, \ \alpha, \beta \in \mathbb{R} $ and $ F : (y,z) \mapsto F(y,z) $ with $ F \in C^2(\mathbb{R}, \mathbb{R}) $. Define
                    $$ \C := \{ u \in C^2 [a,b] : u(a) = \alpha, u(b) = \beta \}, \ J(u) := \int_a^b F(u(x), u'(x)) dx $$
                    and consider the variational problem
                    \begin{equation} \label{varprob}
                        \operatorname{find} \ u \in \C \ \operatorname{such} \operatorname{that} \ J(u) \le J(v), \quad \forall v \in \C.
                    \end{equation}

                    % Part I
                    \begin{enumerate}[(i)]
                        \item Derive the Euler-Lagrange equation associated with the variational problem \eqref{varprob}. (The alternative vanishing lemma may be used without proof.)
                    \end{enumerate}

                    % Answer
                    \begin{itemize}
                        \item Let
                            $$ \C_0 = \{ u \in C^2[a,b] : u(a) = u(b) = 0 \} $$
                            and consider
                            \begin{align*}
                                g(\e) &= J(u+\e w) \\
                                &= \int_a^b F(u+\e w, u' + \e w') \d x
                            \end{align*}
                            for $ \e \in \R, w \in \C_0 $. Then $ u + \e w \in \C $ and $g(\e)$ has a minimum at $ \e=0 $ if $u$ is the solution of the variational problem \eqref{varprob}. Since $ F \in C^2 $, then $ g \in C' $ and
                            \begin{align*}
                                0 &= g'(0) \\
                                &= \left. \frac{\mathrm{d}}{\mathrm{d}\e} \int_a^b F(u+\e w, u'+\e w') \d x \right\vert_{\e=0} \\
                                &= \int_a^b \frac{\p F}{\p y}(u,u')w + \frac{\p F}{\p z}(u,u')w' \d x \\
                                &= \int_a^b \left( \frac{\p F}{\p y}(u,u') - \frac{\mathrm{d}}{\mathrm{d}x} \left( \frac{\p F}{\p z}(u,u') \right) \right) w \d x + \left[ \frac{\p F}{\p z}(u,u')w \right]_{x=a}^b
                            \end{align*}
                            But $ w(a) = w(b) = 0 $, hence
                            \begin{align*}
                                \int_a^b f(x)w(x) \d x &= 0 \\
                                \text{with} \quad f(x) &= \frac{\p F}{\p y}(u,u') - \frac{\mathrm{d}}{\mathrm{d}x} \left( \frac{\p F}{\p z}(u,u') \right) \in C[a,b] \quad \text{and} \quad w \in \C_0
                            \end{align*}
                            By the \textbf{alternative vanishing lemma}, $ f(x) = 0 $, $ \forall x \in [a,b] $. Hence
                            \begin{equation} \tag{*} \label{el}
                                \implies \frac{\p F}{\p y}(u,u') - \frac{\mathrm{d}}{\mathrm{d}x} \left( \frac{\p F}{\p z}(u,u') \right) = 0 \quad \forall x \in [a,b]
                            \end{equation}
                    \end{itemize}

                    % Part II
                    \begin{enumerate}[(i),resume]
                        \item What is the relationship between the solutions and extremals of the variatonal problem \eqref{varprob}?
                    \end{enumerate}

                    % Answer
                    \begin{itemize}
                        \item \textbf{Extremals} are solutions to the Euler-Lagrange equation \eqref{el}
                        \item They are \textbf{candidates} of the solution of the variational problem \eqref{varprob}
                    \end{itemize}

                    % Part III
                    \begin{enumerate}[(i),resume]
                        \item Show that the extremals satisfy
                            \begin{equation*}
                                F(u(x),u'(x)) - u'(x) \frac{\partial F}{\partial z}(u(x), u'(x)) = k,
                            \end{equation*}
                            for some constant $k$.
                    \end{enumerate}

                    % Answer
                    \begin{itemize}
                        \item Let
                            \begin{align*}
                                E(x) &= F(u,u') - u' \frac{\p F}{\p z}(u,u') \\
                                \implies \frac{\mathrm{d}E}{\mathrm{d}x} &= \frac{\p F}{\p y}u' + \frac{\p F}{\p z}u'' - u'' \frac{\p F}{\p z} - u' \frac{\mathrm{d}}{\mathrm{d}x} \left( \frac{\p F}{\p z}(u,u') \right) \\
                                &= u' \left( \frac{\p F}{\p y} - \frac{\mathrm{d}}{\mathrm{d}x}\frac{\p F}{\p z} \right) \\
                                &= 0 \quad \text{by \eqref{el}} \\
                                \implies E(x) &= k \quad \text{constant}
                            \end{align*}
                    \end{itemize}

                % Part B
                \item Two points $ (a,\alpha) \in \mathbb{R}^2 $ and $ (b,\beta) \in \mathbb{R}^2 $ are joined by a curve $ x \mapsto u(x) $ ($u$ being the distance to the x-axis) so that the area of the surface of revolution generated by revolving the curve about the x-axis is minimal. This gives a variational problem of the form \eqref{varprob} with
                    $$ F(y,z) = 2\pi y \sqrt{1+z^2} .$$

                    % Part I
                    \begin{enumerate}[(i)]
                        \item Show that if $u$ is an extremal then
                            $$ u(x) = c_1 \cosh{\left( \frac{x+c_2}{c_1} \right)} ,$$
                            where $ c_1 \ne 0 $ and $c_2$ are arbitrary constants.
                    \end{enumerate}

                    % Answer
                    \begin{itemize}
                        \item Uing (a)(iii)
                            \begin{align*}
                                \frac{\p F}{\p z} &= \frac{2\pi yz}{\sqrt{1+z^2}} \\
                                \implies 2\pi u\sqrt{1+{u'}^2} - u' \frac{2\pi uu'}{\sqrt{1+{u'}^2}} &= k \\
                                \implies 2\pi u &= k \sqrt{1+{u'}^2} \\
                                \implies u'(x) &= \sqrt{ \left( \frac{2\pi u(x)}{k} \right)^2 - 1 } \\
                                &= \sqrt{ \left( \frac{u}{c_1} \right)^2 - 1 }
                            \end{align*}
                            where $ c_1 = \displaystyle\frac{k}{2\pi} $. If $ c_1 = 0 $, then $ u = 0 $ which cannot join two general points. Assume $ c_1 \neq 0 $, then
                            \begin{align*}
                                x+c_2 &= \int \frac{\mathrm{d} u}{\sqrt{ \left( \frac{u}{c_1} \right)^2 - 1 }} \\
                                &= c_1 \int \d v \\
                                &= c_1 v
                            \end{align*}
                            for some arbitrary constant $c_2$. Hence
                            $$ \du{u(x) = c_1 \cosh \left( \frac{x + c_2}{c_1} \right)} $$
                    \end{itemize}

                    % Part II
                    \begin{enumerate}[(i),resume]
                        \item Let $ a = -1, b = 1 $ and $ \alpha = \beta = \sinh{\lambda} $, where $\lambda$ is the positive solution of $ \lambda \tanh{\lambda} = 1 $. Determine $c_1$ and $c_2$.
                    \end{enumerate}

                    % Answer
                    \begin{itemize}
                        \item
                            \begin{align*}
                                u(-1) &= \sinh \lambda \\
                                \implies c_1 \cosh \left( \frac{c_2-1}{c_1} \right) &= \sinh \lambda \\
                                &= \frac{1}{\lambda} \cosh \lambda \\
                                \text{and} \quad u(1) &= \sinh \lambda \\
                                \implies c_1 \cosh \left( \frac{c_2+1}{c_1} \right) &= \sinh \lambda \\
                                &= \frac{1}{\lambda} \cosh \lambda \\
                                \therefore \du{c_2 = 0} \quad &\text{and} \quad \du{c_1 = \frac{1}{\lambda}}
                            \end{align*}
                    \end{itemize}
            \end{enumerate}

% End Questions
\end{enumerate}

\end{document}
